#!/usr/bin/python3
import time
from time import localtime

from mqtt_endpoint import MQTTEndpoint
from chirpstack_endpoint import ChirpstackEndpoint
from struct_adapter import StructAdapter
from panel_proxy import PanelProxy

class Backend:
    def sendStatus(self, status):
        print("sendStatus: {}".format(status))
        return 

    def notify(self, notification):
        print("notify: {}".format(notification))

if __name__ == '__main__':
    # backend service    
    backend = Backend()

    mqtt_ep = MQTTEndpoint()
    chirpstack_ep = ChirpstackEndpoint()

    adapter = StructAdapter([mqtt_ep, chirpstack_ep])
    adapter.register_service(("backend", backend))
    adapter.enable()

    panel_mqtt = PanelProxy(adapter, 'mqtt:panel', 'panel')
    panel_lora = PanelProxy(adapter, 'chirp:70b3d54996f483d4', 'panel')
    
    counter = 0

    while True:
        print("{:02d}:{:02d}:{:02d} PROXY REQUEST: setPictogram({})".format(localtime()[3], localtime()[4], localtime()[5], counter))
        panel_lora.setPictogram(counter)
        #print(panel_lora.getStatus())

        counter += 1
        if counter == 5:
            counter = 0

        time.sleep(30)
    
sys.exit(0)
