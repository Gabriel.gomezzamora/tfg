import threading
import struct
from time import sleep
from timeit import default_timer as timer

REQUEST_TIMEOUT = 5
REQUEST_SLEEP = 0.25

class PanelProxy:
    def __init__(self, adapter, string_proxy, dst):
        self.lock = threading.Lock()
        self.protocol = string_proxy.split(':')[0]
        self.url = string_proxy.split(':')[1]
        self.dst = dst
        self.adapter = adapter
        self.pending = False
        self.result = None

        self.adapter.register_proxy(self.protocol, self.url)

    def setPictogram(self, pictogram):
        args = struct.pack("!B", pictogram)        
        #timestamp = utime.time()
        ret = self.request("setPictogram", args)
        #print("request time: {}".format(utime.time() - timestamp))
        return ret

    def getStatus(self):
        ret = self.request("getStatus", b'0')
        return ret

    def request(self, op, args):
        self.lock.acquire()
        self.pending = True
        request_time = timer()
        self.adapter.request(self, op, args)
        self.lock.release()
        delay = 0
        while self.pending:
            if delay >= REQUEST_TIMEOUT:
                print("   timeout")
                return False
            delay += REQUEST_SLEEP
            sleep(REQUEST_SLEEP)
        print("   latency: {}".format(timer() - request_time))            
        return self.result

    def reply(self, msg):
        self.lock.acquire()
        self.pending = False
        self.result = struct.unpack("B", msg)[0]
        self.lock.release()
