BROKER_ADDR = "192.168.1.60"
BROKER_USER = 'mosquitto'
BROKER_PWD = 'mosquitto'

service_id = { 
    "backend": (0, {
        "sendStatus": 0,
        "notify": 1
        }),
    "panel" : (1, {
        "setPictogram": 0,
        "getStatus": 1
        })
}

service_name = { 
    0: ("backend", {
        0: "sendStatus",
        1: "notify"
        }),
    1 : ("panel", {
        0: "setPictogram",
        1: "getStatus"
        })
}
