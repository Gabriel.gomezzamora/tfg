import struct
from time import sleep

import config
from mqtt_endpoint import MQTTEndpoint
from lora_endpoint import LoRaEndpoint
from struct_adapter import StructAdapter
from backend_proxy import BackendProxy
from panel import Panel



# endpoints definition
lora_ep = LoRaEndpoint()
mqtt_ep = MQTTEndpoint()

# the adapter includes all defined endpoints
adapter = StructAdapter([lora_ep, mqtt_ep])
sleep(1)

# proxies definition
backend_lora = BackendProxy(adapter, 'lora:backend')
backend_mqtt = BackendProxy(adapter, 'mqtt:backend')

# servers
panel = Panel()

adapter.register_service(("panel", panel))
adapter.enable()

# first message to enable downlink
result = backend_lora.sendStatus(1)

while True:
    result = backend_lora.sendStatus(panel.getStatus(''))
    #print("LoRa sendStatus result: {}".format(result))

    #result = backend_mqtt.sendStatus(1)
    #print("MQTT sendStatus result: {}".format(result))

    sleep(30)
