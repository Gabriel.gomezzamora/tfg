import time
import socket
import _thread
import ubinascii
from network import LoRa

import config

class LoRaEndpoint:
    def __init__(self):
        self.type = 'lora'

        self.cb = None
        self.recv_thread = None
        self.socket = None
        self.lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, device_class=LoRa.CLASS_C)

    def connect(self, cb):
        print("LoRaWAN joining ...")
        # join a network using OTAA
        self.lora.join(activation=LoRa.OTAA, auth=(config.APP_EUI, config.APP_SWKEY))

        for i in range(5): # 10 seconds timeout
            if self.lora.has_joined():
                break
            time.sleep(2)
        print("joined")
        print("device EUI: {}".format(ubinascii.hexlify(LoRa().mac())))

        # create a LoRa socket
        self.socket = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        self.socket.bind(1)
        self.socket.setblocking(False)

        self.cb = cb

    def enable_reception(self):
        if not self.recv_thread:
            self.recv_thread = _thread.start_new_thread(self.recv, ())
            print("LoRaWAN recv thread enabled")    

    def request(self, dst, pkt):
        self.send(dst, pkt)

    def reply(self, dst, pkt):
        self.send(dst, pkt)

    def send(self, dst, pkt):
        try:
            print("LoRaWAN uplink: {}".format(pkt))
            self.socket.send(pkt)
        except Exception as ex:
            print("EXCEPTION uplink: {}".format(ex))

    def recv(self):
        while True:
            pkt = self.socket.recv(256)
            if len(pkt) > 0:
                print("LoRAWAN downlink: {}".format(pkt))            
                if self.cb:
                    self.cb(self, pkt, '')
            time.sleep(0.25)

    # in LoRa no need to register. Messages are broadcasted
    def register_proxy(self, url):
        pass

    def register_service(self, url):
        pass
