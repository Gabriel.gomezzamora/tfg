import struct
from machine import UART
from time import sleep

# B: 0xaa, B: 0x55, B: command, B: pictogram, B: 0xd
_SERIAL_PKT_FORMAT = "!BBBBB"

SET_BACKLIGHT_CMD = 0
CLEAR_SCREEN_CMD = 1
SET_PICTOGRAM_CMD = 2
GET_PICTOGRAM_ID_CMD = 3


def send_serial_command(command, arg):
    packet = struct.pack(_SERIAL_PKT_FORMAT, 0x55, 0xaa, command, arg, 0xd)
    uart.write(packet)

def get_current_pictogram():
    send_serial_command(GET_PICTOGRAM_ID_CMD, 0)
    sleep(0.5)
    reply = uart.read(4)
    if reply != None:
        return reply[2]
    else:
        return -1

# configure the UART
uart = UART(1, baudrate = 115200)

class Panel:
    def __init__(self):
        self.current_pictogram = 0

    def setPictogram (self, pictogram):
        self.current_pictogram = pictogram
        print("    panel pictogram set to {}".format(pictogram))
        send_serial_command(SET_PICTOGRAM_CMD, int.from_bytes(pictogram, 'big'))
        
    def getStatus(self, dummy):
        print("    getStatus")
        #self.current_pictogram += 1
        #return self.current_pictogram
        return get_current_pictogram()
