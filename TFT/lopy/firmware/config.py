import ubinascii

WIFI_SSID = "b8"
WIFI_PW = "12*12=144"
SERVER_IP = "192.168.1.60"
SERVER_PORT = 23
BROKER_ADDR = "192.168.1.60"
BROKER_USER = "mosquitto"
BROKER_PWD = "mosquitto"

APP_EUI = ubinascii.unhexlify('ADA4DAE3AC12676B')
APP_SWKEY = ubinascii.unhexlify('d3a46f74d6b987c7ceb7c03e1ccfd5cc')

service_id = { 
    "backend": (0, {
        "sendStatus": 0,
        "notify": 1
        }),
    "panel" : (1, {
        "setPictogram": 0,
        "getStatus": 1
        })
}

service_name = { 
    0: ("backend", {
        0: "sendStatus",
        1: "notify"
        }),
    1 : ("panel", {
        0: "setPictogram",
        1: "getStatus"
        })
}
