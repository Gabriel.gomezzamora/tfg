import _thread
import machine
import time
import sys
import pycom
from network import WLAN
from mqtt import MQTTClient

import config

RED = 0xFF0000
YELLOW = 0xFFFF33
GREEN = 0x007F00
OFF = 0x000000

def set_led_to(color=GREEN):
    pycom.heartbeat(False) # Disable the heartbeat LED
    pycom.rgbled(color)

def flash_led_to(color=GREEN, t1=1):
    set_led_to(color)
    time.sleep(t1)
    set_led_to(OFF)

def connect_to_wifi(wifi_ssid, wifi_passwd):
        wlan = WLAN(mode=WLAN.STA)

        for ltry in range(3):
            print("Connecting to: "+wifi_ssid+". Try "+str(ltry))
            #nets = wlan.scan()
            #for net in nets:
            #    if net.ssid == wifi_ssid:
            #        print('Network '+wifi_ssid+' found!')
            #wlan.connect(net.ssid, auth=(net.sec, wifi_passwd), timeout=5000)
            wlan.connect(wifi_ssid, auth=(WLAN.WPA2, wifi_passwd), timeout=5000)
            while not wlan.isconnected():
                machine.idle() # save power while waiting
            print('WLAN connection succeeded!')
            flash_led_to(GREEN, 1)
            print (wlan.ifconfig())
                    #break

            if wlan.isconnected():
                break
            else:
                print('Cannot find network '+wifi_ssid)
                flash_led_to(RED, 1)
                sleep(1)
        
        if not wlan.isconnected():
            print('Cannot connect to network '+wifi_ssid+'. Quitting!!!')
            sys.exit(1)
        
        return wlan

class MQTTEndpoint:
    def __init__(self):
        self.type = 'mqtt'

        self.recv_thread = None
        self.wlan = connect_to_wifi(config.WIFI_SSID, config.WIFI_PW)
        self.client = None
        self.connected = False

    def connect(self, cb):
        self.client = MQTTClient('id',config.BROKER_ADDR,port=1883, user=config.BROKER_USER, 
            password=config.BROKER_PWD)        
        self.cb = cb
        self.client.set_callback(self.on_message)
        self.connected = not self.client.connect()
        if self.connected :
            print ("Connected to broker: "+ config.BROKER_ADDR)

    def on_message(self, topic, msg):
        reply_channel = topic.decode("utf-8","ignore").split('/')[-2]
        self.cb(self, msg, reply_channel)        

    # The thread can only be enabled after all subscriptions have been done
    def enable_reception(self):
        if not self.recv_thread:
            self.recv_thread = _thread.start_new_thread(self.recv, ())
            print("MQTT recv thread enabled")

    def request(self, dst, pkt):
        try:
            print("MQTT uplink proxy request: {} / {}".format(dst, pkt))
            self.client.publish(dst + '/rx', pkt)
        except Exception as ex:
            print("EXCEPTION MQTT for dst {} : {}".format(dst, ex))

    def reply(self, dst, pkt):
        try:
            print("MQTT uplink service reply: {} / {}".format(dst, pkt))
            self.client.publish(dst + '/tx', pkt)
        except Exception as ex:
            print("EXCEPTION MQTT for dst {} : {}".format(dst, ex))
    
    def recv(self):
        while True:
            self.client.wait_msg()
            #self.rx += 1
    
    def register_proxy(self, url):
        self.client.subscribe(topic = url + '/tx')
        print("mqtt proxy subscription to {}".format(url+'/tx'))

    def register_service(self, url):
        self.client.subscribe(url + '/rx')
        print("mqtt service subscription to {}".format(url+'/rx'))