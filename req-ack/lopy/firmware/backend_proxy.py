import _thread
import struct
import utime
from time import sleep

REQUEST_TIMEOUT = 5
REQUEST_SLEEP = 0.25

# string_proxy format: protocol:url, where url is a path with '/' separator
#                      the last part of the path must be the destination id (dst)
#            examples: 'mqtt:some/path/to/destination' where destination is the dst id
#                      'lora:destination'
class BackendProxy:
    def __init__(self, adapter, string_proxy):
        self.lock = _thread.allocate_lock()        
        self.protocol = string_proxy.split(':')[0]
        self.url = string_proxy.split(':')[1]
        self.dst = self.url.split('/')[-1]
        self.adapter = adapter
        self.pending = False
        self.result = None

        self.adapter.register_proxy(self.protocol, self.url)

    def sendStatus(self, status):
        args = struct.pack("!B", status)        
        timestamp = utime.time()
        ret = self.request("sendStatus", args)
        print("   request time: {}".format(utime.time() - timestamp))
        return ret

    def request(self, op, args):
        self.lock.acquire()
        self.pending = True
        self.adapter.request(self, op, args)
        self.lock.release()
        delay = 0
        while self.pending:
            if delay >= REQUEST_TIMEOUT:
                print("   timeout")
                return False
            delay += REQUEST_SLEEP
            sleep(REQUEST_SLEEP)
        return True

    def reply(self, msg):
        self.lock.acquire()
        self.pending = False
        self.result = msg
        self.lock.release()
