
class Panel:
    def __init__(self):
        self.current_pictogram = 0

    def setPictogram (self, pictogram):
        self.current_pictogram = pictogram
        print("    panel pictogram set to {}".format(pictogram))
        
    def getStatus(self, dummy):
        print("    getStatus")
        self.current_pictogram += 1
        return self.current_pictogram
