import paho.mqtt.client
import threading
import time
import json
import base64

import config

class ChirpstackEndpoint:
    def __init__(self):
        self.type = 'chirp'

        self.client = paho.mqtt.client.Client(client_id='chrp_loopback', clean_session=True)
        self.client.username_pw_set(config.BROKER_USER, config.BROKER_PWD)
        self.client.on_connect = self.on_connect
        self.client.on_message = None
        self.connected = False
        
    def connect(self, cb):        
        self.client.connect_async(host=config.BROKER_ADDR, port=1883)
        self.cb = cb
        self.client.on_message = self.on_message
        self.client.loop_start()
        return

    def on_connect(self, client, userdata, flags, rc):
        self.connected = True
        print('connected (%s)' % client._client_id)

    def on_message(self, client, userdata, msg):
        data = json.loads(msg.payload)
        reply_channel = msg.topic.split('/')[-2]
        msg.payload = base64.b64decode(data['data'])
        self.cb(self, msg.payload, reply_channel)

    # Connection enabled by default
    def enable_reception(self):
        pass

    def request(self, dst, pkt):
        msg = { "confirmed": False, "fPort": 1, 
            "data": base64.b64encode(pkt).decode('ascii') }
        print("LoRa downlink proxy request: {} / {}".format('application/1/device/' + dst, msg))

        try:
            self.client.publish('application/1/device/' + dst + '/tx', json.dumps(msg))
        except Exception as ex:
            print("EXCEPTION LoRA for dst {} : {}".format(dst, ex))

    def reply(self, channel, pkt):
        msg = { "confirmed": False, "fPort": 1, 
            "data": base64.b64encode(pkt).decode('ascii') }
        dst = 'application/1/device/' + channel + '/tx'
        print("LoRa downlink service reply: {} / {}".format(dst, pkt))
        try:
            self.client.publish(dst , json.dumps(msg))
        except Exception as ex:
            print("EXCEPTION in lora send: {} ".format(ex))

    def register_proxy(self, url):        
        while not self.connected:
            time.sleep(1)

        dst = 'application/1/device/' + url + '/rx'
        self.client.subscribe(topic = dst)
        print("LoRa proxy subscription to {}".format(dst))

    def register_service(self, url):
        while not self.connected:
            time.sleep(1)

        #dst = 'application/1/device/' + url + '/rx'
        dst = 'application/1/device/+/rx'
        self.client.subscribe(dst)
        print("LoRa service subscription to {}".format(dst))