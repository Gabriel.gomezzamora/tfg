import struct
import config

# H: message id, B: type (request-> 0, reply -> 1), B: destination
MSG_HEADER = "!HBB"

class StructAdapter:
    def __init__(self, endpoints):
        self.endpoints = {}
        for ep in endpoints:            
            self.endpoints[ep.type] = ep
            ep.connect(self.on_message)
        self.id = 0
        self.last_request = (0, None)
        self.services = {}

    def enable(self):
        for protocol, ep in self.endpoints.items():
            ep.enable_reception()

    # msg encoded
    def request(self, proxy, op, args):
        service = config.service_id[proxy.dst]
        service_code = service[0]
        op_code = service[1][op]
        pkt = struct.pack(MSG_HEADER, self.id, 0, service_code) + struct.pack("B", op_code) + args
        try:            
            ep = self.endpoints[proxy.protocol]
            ep.request(proxy.url, pkt)
            self.last_request = (self.id, proxy)
            self.id += 1
        except Exception as ex:
            print('EXCEPTION in adapter request: {}'.format(ex))


    def reply(self, endpoint, reply_channel, id, dst, ret):
        reply = struct.pack(MSG_HEADER, id, 1, dst) + struct.pack("B", ret)
        endpoint.reply(reply_channel, reply)

    def on_message(self, ep, msg, reply_channel):
        print("incoming msg: {} / {} / {}".format(ep.type, reply_channel, msg))

        id, type, dst = struct.unpack(MSG_HEADER, msg[:4])
        
        # reply message
        if type == 1:        
            if id == self.last_request[0]:
                print("    incoming reply forwarded to : {}".format(self.last_request[1]))
                # destination code not used
                self.last_request[1].reply(msg[4:])

        # incoming request
        else:
            op_code = msg[4]
            args = msg[5:]
            print("    opcode: {}, args: {}".format(op_code, args))

            service = config.service_name[dst]
            service_name = service[0]
            service_instance = self.services[service_name]
            op_name = service[1][op_code]

            if op_name in dir(service_instance):
                f = getattr(service_instance, op_name)
                ret = f(args)

                # FIXME: return value always a byte
                if ret == None:
                    ret = 0

                self.reply(ep, reply_channel, id, dst, ret)

             
    def register_service(self, service):
        for protocol, ep in self.endpoints.items():            
            ep.register_service(service[0])
        self.services[service[0]] = service[1]

    def register_proxy(self, protocol, url):
        ep = self.endpoints[protocol]
        ep.register_proxy(url)
