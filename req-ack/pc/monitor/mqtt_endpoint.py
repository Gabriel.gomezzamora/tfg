import paho.mqtt.client
import threading
import config
import time

class MQTTEndpoint:
    def __init__(self):
        self.type = 'mqtt'

        self.client = paho.mqtt.client.Client(client_id='loopback', clean_session=False)
        self.client.username_pw_set(config.BROKER_USER, config.BROKER_PWD)
        self.client.on_connect = self.on_connect
        self.client.on_message = None
        self.connected = False

    def connect(self, cb):        
        self.client.connect_async(host=config.BROKER_ADDR, port=1883)
        self.cb = cb
        self.client.on_message = self.on_message
        self.client.loop_start()

    def on_connect(self, client, userdata, flags, rc):
        self.connected = True
        print('connected (%s)' % client._client_id)

    def on_message(self, client, userdata, msg):
        reply_channel = msg.topic.split('/')[-2]
        self.cb(self, msg.payload, reply_channel)

    # Connection enabled by default
    def enable_reception(self):
        pass

    def request(self, dst, pkt):
        try:
            print("MQTT uplink proxy request: {} / {}".format(dst, pkt))
            self.client.publish(dst + '/rx', pkt)
        except Exception as ex:
            print("EXCEPTION MQTT for dst {} : {}".format(dst, ex))

    def reply(self, dst, pkt):
        try:
            print("MQTT uplink service reply: {} / {}".format(dst, pkt))
            self.client.publish(dst + '/tx', pkt)
        except Exception as ex:
            print("EXCEPTION MQTT for dst {} : {}".format(dst, ex))

    def register_proxy(self, url):
        self.client.subscribe(topic = url + '/tx')
        print("mqtt proxy subscription to {}".format(url+'/tx'))

    def register_service(self, url):
        self.client.subscribe(url + '/rx')
        print("mqtt service subscription to {}".format(url+'/rx'))