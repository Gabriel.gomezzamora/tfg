#!/usr/bin/python3
import paho.mqtt.client as mqtt
import argparse
import time
import sys 
import json
import threading

broker_address = "localhost"
broker_port = 1883
mensajes = 5
contador = 0
mensajes_ini = 0
mensajes_fin = 0
estado = False
topic_recepcion_fernando = 'rx'
topic_recepcion_gabriel= 'up'
def on_message(client, userdata, message):

   #print("Mensaje recibido=", str(message.payload.decode("utf-8")))
   #print("Topic=", message.topic)
   global estado
   global contador
   global mensajes_ini
   global mensajes_fin
  
   if(message.topic[len(message.topic)-2:] == topic_recepcion_fernando ):
      #print("tipos de evento:",message.topic[len(message.topic)-2:]) 
      print("Topic=", message.topic)
      contador += 1
   if(message.topic[0:12] == "respuesta/up"):
      print("Topic=", message.topic)
      if(not estado):
         mensajes_ini += int(str(message.payload.decode("utf-8")))         
      else:
         mensajes_fin += int(str(message.payload.decode("utf-8")))
         

   print("Contador=",contador) 
def on_connect(client, userdata, flags, rc):
   print("Conectado")
    
def subcribe(topic_suscribirse,topic_publicar,mensaje_publicar,tiempo):
   
   client.loop_start()
   client.subscribe(topic_suscribirse) # Subscripción al topi  
   client.publish(topic_publicar,mensaje_publicar)      
   time.sleep(tiempo)
   client.loop_stop()
  

     
      
   

if __name__ == '__main__':  
   topic  = ["respuesta/up/+","application/1/device/#"]
   parser = argparse.ArgumentParser()
   parser.add_argument("tiempo" , help="Introduce el tiempo en segundos que van a durar las pruebas.")
   parser.add_argument("cycle_time" , help="Introduce el tiempo de ciclo de cada mensaje en segundos")
   parser.add_argument("nodo" , help="Introduce el numero de nodo para enviar preubas a un solo nodo o dejalo en blanco para todos los nodos")
  
   args = parser.parse_args()
   tiempo = int(args.tiempo)
   cycle_time = int(args.cycle_time)
   nodo = int(args.nodo)

   if(nodo == 0):
      nodo = ""
   client = mqtt.Client('Cliente1') 
   client.on_message = on_message    
   client.connect(broker_address, broker_port, 60) 



   subcribe(topic[0],"lopy/peticion",'{"nodo":"'+str(nodo)+'","clave":9}',5)
   print("mensajes ini =",mensajes_ini)
   estado = True
   subcribe(topic[1],"lopy/peticion",'{"nodo":"'+str(nodo)+'","clave":5,"cycle_time":'+str(cycle_time)+',"from":"now","mensaje":"hola"}',tiempo)
   client.publish("lopy/peticion",'{"nodo":"","clave":6}')
   subcribe(topic[0],"lopy/peticion",'{"nodo":"'+str(nodo)+'","clave":9}',5)
  
   print("Mensajes inicio:",mensajes_ini)
   print("Mensajes fin:",mensajes_fin)
   mensajes_enviados = mensajes_fin-mensajes_ini
   porcentaje_llegados = (contador/mensajes_enviados)
   
   print("------------------")
   
   print("Mensajes enviados:",mensajes_fin-mensajes_ini)
   print("Mensajes que han llegado:", contador)
   print("Porcentaje mensajes llegados:", round((porcentaje_llegados*100),2), "%")
   print("Procentaje mensajes perdidos",round(((1-porcentaje_llegados)*100),2),"%")
   print("------------------")
    
            
   print("Fin")
   




sys.exit(0)
