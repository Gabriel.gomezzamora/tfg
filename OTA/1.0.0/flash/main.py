#!/usr/bin/env python
#
# Copyright (c) 2019, Pycom Limited.
#
# This software is licensed under the GNU GPL version 3 or any
# later version, with permitted additional terms. For more information
# see the Pycom Licence v1.0 document supplied with this file, or
# available at https://www.pycom.io/opensource/licensing
#
#VERSION 1.0.3
from network import LoRa, WLAN
import socket
from OTA import WiFiOTA
from time import sleep
import binascii
from mqtt import MQTTClient
from config import WIFI_SSID, WIFI_PW, SERVER_IP, BROKER_ADDR, connect_to_wifi,YELLOW,set_led_to
from machine import Timer
import time
import utime
import pycom
from lora import LoRa_config
from ntp import sincronizacion_en_tiempo
from ntp import timezone
import _thread
import json



from OTA_VERSION import VERSION

id_nodo = '1'
# Try to get version number
try:
    from NODO import ID_NODO
except ImportError:
    ID_NODO = id_nodo


def mqtt_thread():
    global client
    
    print('Waiting messages...')
    while 1:
        #escuchando_LoRa()
        client.wait_msg()
        #client.check_msg()
       
   
        


def actualizacionOTA():
    global ota
    print("Actualizando...")
   
    try:
        ota.connect()
        ota.update()
        return "Es posible que ya este en la ultima version:"+VERSION
    except Exception as ex:
        print('ERROR de conexio o alcualizacion en el serviror'.format(ex))    
        return 'ERROR de conexio o alcualizacion en el serviror: {}'.format(ex)
    

def reply(topic,msg):
    client.publish(topic,msg)
    
def on_message(topic, msg):
    
    global ejecucion_ciclica
    global ejecucion_ciclica_id
    global ejecucion_ciclica_mensaje
    global ejecucion_ciclica_cycle
    global ejecucion_ciclica_at

    msg_json = None
    nodo = None
    opcion_elegida = None
   

    try:
        #m_in =json.dumps(msg) #decodificar datos json
        cadena = str(msg.decode("utf-8","ignore"))
        msg_json = json.loads(cadena)
        nodo = msg_json["nodo"]    
        opcion_elegida = msg_json["clave"]
    except:
        print("Error en al campo json nodo o clave")
        return
  
    if(nodo != ID_NODO and nodo != ""):
        return
    
    print("valor:",msg_json["clave"])
    print("nodo:",msg_json["nodo"])

    ## Actualizacion via OTA
    if(1 == opcion_elegida):
        
        print("Elegida opcion 1 ==> Se procedera a actualizar el software de lopy.")
        salida = actualizacionOTA()        
        reply("respuesta","Nodo:"+ID_NODO+" "+str(salida))
      
    elif(2 == opcion_elegida):
        print("Enviando mensaje downlink.."+lora_config.identificador_LoRa)
        downlink_fernando = "application/1/device/"+lora_config.identificador_LoRa+"/tx"
        downlink_gabriel = 'application/1/device/'+lora_config.identificador_LoRa+'/command/down'

        lora_config.envio_downlink(downlink_fernando,'{"confirmed": false, "fPort": 1, "data":"hola"}')
    elif(3 == opcion_elegida):
        try:
            nombre_wifi = wlan.ssid()
            autenticacion = wlan.auth()
            reply("respuesta","Nodo:"+ID_NODO+" "+'SSID: '+ str(nombre_wifi)+" autenticacion : "+ str(autenticacion) )
        except Exception as ex:
            reply("respuesta","Nodo:"+ID_NODO+" fallo en campo json" )
            print('EXCEPTION fallo en datos consultados{}'.format(ex))
    elif(4 == opcion_elegida):
        #Mandar mensaje LoRa con un retarde de segundos indicado o inmediatamente      
        try:
            to = msg_json["retardo"]
            mensaje = msg_json["mensaje"]
           
            lora_config.send_LoRa_message(mensaje,to)
            reply("respuesta","Nodo:"+ID_NODO+" ha realizado el envio de un mensaje que comenzara a enviarse en "+str(to)+" segundos")
        except Exception as ex:
            reply("respuesta","Nodo:"+ID_NODO+" fallo en campo json" )
            print('EXCEPTION fallo en json {}'.format(ex))
       
    elif(5 == opcion_elegida):
        if(not lora_config.estado_send_cyclic_message):
            try:
                print("ID:",lora_config.mensaje_id_count)
                #Mandar mensaje LoRa de forma cyclica , cada x seg indicados.
                cycle_time = msg_json["cycle_time"]
                _from = msg_json["from"]        #Puede ser "now" o un numero
                mensaje = msg_json["mensaje"]
               
                error_hora = -1
                if(_from != "now"):
                    hora = msg_json["hora"]
                    min = msg_json["min"]
                    
                    error_hora = lora_config.programar_tiempo(hora,min)
                    print("#####Error:",error_hora)
                    if(error_hora != -1):
                        reply("respuesta","Nodo "+ID_NODO+": Se ha  programado el envio de un mensajes de forma ciclica cada "+str(cycle_time)+
                    "segundos que comenzara a enviarse a las "+str(hora)+":"+str(min) +" del dia de hoy.")
                    else:
                        reply("respuesta","Nodo "+ID_NODO+":  Error la hora de comienzo "+str(hora)+":"+str(min)+" es inferior a la hora actual")

                else:
                    error_hora = utime.time()+ timezone

                    reply("respuesta","Nodo "+ID_NODO+": Se ha  programado el envio de un mensajes de forma ciclica cada "+str(cycle_time)+" segundos que comenzara a enviarse ahora")
                print("Antes de if:",error_hora)   
                if(error_hora != -1):
                    print("Error hora:",error_hora)
                    ejecucion_ciclica = True
                    ejecucion_ciclica_mensaje = mensaje
                    ejecucion_ciclica_cycle = cycle_time
                    ejecucion_ciclica_at = error_hora
                    #lora_config.send_LoRa_message_every(Mensaje.id_count,mensaje,cycle_time,error_hora)
            except Exception as ex:
                reply("respuesta","Nodo:"+ID_NODO+" fallo en campo json" )
                print('EXCEPTION fallo en json {}'.format(ex))
        else:
            print("Error: ya hay una evento programado no puede haber dos a la vez.")
            reply("respuesta","Nodo "+ID_NODO+": Error: ya hay una evento programado no puede haber dos a la vez.")
    elif(6 == opcion_elegida):
        try:
            #_thread.start_new_thread(lora_config.cancel_LoRa_cyclic_message(),())
            ejecucion_ciclica = False            
            error=lora_config.cancel_LoRa_cyclic_message()
            print("Cancelada:",error)
            if(error == -1):
                reply("respuesta","Nodo:"+ID_NODO+" no se puede cancelar el envio, no se esta enviando nada.")
            else:
               
                reply("respuesta","Nodo:"+ID_NODO+" ha cancelado el envio ciclico.")
        except Exception as ex:
                reply("respuesta","Nodo:"+ID_NODO+" fallo en campo json" )
                print('EXCEPTION fallo en json {}'.format(ex))
    elif(7 == opcion_elegida):
        try:
            ##on/off  modo debugin
            estado = msg_json["estado"]
            print("Estado:",estado)
            lora_config.debug_LoRa(estado)
            reply("respuesta","Nodo:"+ID_NODO+" ha puesto en modod ["+estado+"] la depuracion")
            
        except Exception as ex:
                reply("respuesta","Nodo:"+ID_NODO+" fallo en campo json" )
                print('EXCEPTION fallo en json {}'.format(ex))
    elif(9 == opcion_elegida):
        try:
            try:
                datos = lora_config.get_LoRa_status()
                mensajes_enviados = datos[0]
                mensaje_recibidos = datos[1]
            except Exception as ex:
                reply("respuesta","Nodo:"+ID_NODO+" fallo en campo json" )
                print('EXCEPTION fallo fallo vector{}'.format(ex))  
            
            reply("respuesta",'{"Nodo:"'+ID_NODO+'",mensaje_enviados":'+str(mensajes_enviados)+',"mensaje_recibidos":'+str(mensaje_recibidos)+'"}')
            reply("respuesta/up/"+ID_NODO,str(mensajes_enviados))
            reply("respuesta/down/"+ID_NODO,str(mensaje_recibidos))

        except Exception as ex:
                reply("respuesta","Nodo:"+ID_NODO+" fallo en campo json" )
                print('EXCEPTION fallo en json {}'.format(ex))                           
    else:
        print("Opcion incorrecta.")       
      

############################# INICIO DEL PROGRAMA: ###############################
#Setup OTA

ota = WiFiOTA(WIFI_SSID,
              WIFI_PW,
              SERVER_IP,  # Update server address
              8008)  # Update server port
wlan = connect_to_wifi(WIFI_SSID,WIFI_PW)

#Sincronizacion de tiempo para todos los nodos.
tiempo = sincronizacion_en_tiempo()


id_mqtt = "mqtt_id_"+ID_NODO

#Creo el cliente mqtt
client = MQTTClient(id_mqtt,BROKER_ADDR,port=1883)
client.set_callback(on_message)

if not client.connect():
    print ("Connected to broker: "+BROKER_ADDR)
client.subscribe('lopy/peticion')


lora_config = LoRa_config(client)
thread_mqtt_1 = _thread.start_new_thread(mqtt_thread,())


ejecucion_ciclica = False

ejecucion_ciclica_mensaje = ""
ejecucion_ciclica_cycle = 0
ejecucion_ciclica_at = 0

while True:
    if ejecucion_ciclica:
        lora_config.send_LoRa_message_every(ejecucion_ciclica_mensaje,ejecucion_ciclica_cycle,ejecucion_ciclica_at)
    else:
        sleep(0.1)



'''mosquitto_pub -u mosquitto -P mosquitto -t 'application/1/device/70b3d549944573df/command/down' -m '{"confirmed": false, "fPort": 1, "data":"hola"}' '''
'''mosquitto_pub -h localhost  -p 1883 -u mosquitto -P mosquitto -t "application/1/device/70b3d5499458696e/tx"  -m '{"confirmed":false, "fPort":1, "data":"AQID"}'''
