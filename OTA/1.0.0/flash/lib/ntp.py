from network import WLAN
import time
import machine

timezone = 1*3600

def sincronizacion_en_tiempo():
   
    rtc = machine.RTC()
    rtc.ntp_sync("pool.ntp.org",1200)       #Se sincronizara cada 20 min, si no pongo parametro 1200 sera cada uno por defecto.
    while not rtc.synced():
        machine.idle()
    print("RTC synced with NTP time")
    #adjust your local timezone, by default, NTP time will be GMT
    #time.timezone(2*60**2) #we are located at GMT+2, thus 2*60*60
    #time.timezone(1*60**2) #GMT +1  que es la zona horaria donde estamos.
    
    #return time.localtime()
    return time