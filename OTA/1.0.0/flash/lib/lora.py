from network import LoRa
import socket
import ubinascii
import struct
import binascii
import time
import utime
from time import sleep
import _thread
from machine import Timer
from machine import RTC
import machine
from ntp import timezone
import _thread
import NODO



chrono = Timer.Chrono()
TOPIC_SEND_MESSAGE_LORA = "device/ /send_message_LoRa"
TOPIC_RECEIVE_MESSAGE_LORA = "device/ /receive_message_LoRa"
class LoRa_config:
    
    respuesta = ""
    def __init__(self,mqtt_client):
        self.conexion_LoRa = None
        self.identificador_LoRa = None
        self.estado_send_cyclic_message = False #Para saber si ya hay un envio programado, no podemos tener programados dos enviosen paralelo.
        self.debug_mode = "off"
        self.mqtt_client = mqtt_client
        self.inicio_conexion_LoRa()
        self.mensajes_enviados = 0
        self.mensajes_recibidos = 0
        self.mensaje_id_count = 0
    
        
    def inicio_conexion_LoRa(self):
        
        self.lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868,device_class=LoRa.CLASS_C)      
        self.identificador_LoRa=binascii.hexlify(self.lora.mac()).decode('utf-8')
        #self.lora.join(activation=LoRa.ABP, auth=(NODO.dev_addr, NODO.nwk_swkey, NODO.app_swkey))
        self.lora.join(activation=LoRa.OTAA, auth=(NODO.APP_EUI, NODO.APP_SWKEY), timeout=0)
        # wait until the module has joined the network
        while not self.lora.has_joined():
            time.sleep(2.5)
            print('Not yet joined...')

        print('Joined')
        print("Lora has joinde = ",self.lora.has_joined())
        
        # create a LoRa socket
        self.conexion_LoRa = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

        # set the LoRaWAN data rate
        self.conexion_LoRa.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
        #comezar a escuchar por LoRa
        thread_LoRa = _thread.start_new_thread(self.escuchando_LoRa,())

    def envio_downlink(self,topic,msg):
        print("Topic:",str(topic)+" mensaje:"+str(msg))
        ##Medir tiempo de envio
        chrono.reset()
        chrono.start()
        self.mqtt_client.publish(topic,msg)  
       
    def escuchando_LoRa(self):
        print("Escuchando LoRa..")
        while True:
            data = self.conexion_LoRa.recv(64)
            
            if(data != b''):
                chrono.stop()
                tiempo_transcurrido=chrono.read()
                print("FIN->",tiempo_transcurrido)
                print("Escuchando LoRa Dato recibido: ",data)
               
                self.mensaje_id_count =+ 1
                self.mensajes_recibidos += 1
                self.mqtt_client.publish(TOPIC_RECEIVE_MESSAGE_LORA[:7]+NODO.ID_NODO+TOPIC_RECEIVE_MESSAGE_LORA[8:],
                '{"nodo":'+str(NODO.ID_NODO)+'"id":'+str(self.mensaje_id_count)+',"msg_data":'+str(data)+',"latencia":'+str(tiempo_transcurrido)+'}')
                
                #Si esta activo el modod debug 
                if(self.debug_mode == "on"):
                    self.debuging_LoRa("downlink","Datos recividos sin decodificar")
                data = b''
            time.sleep(0.25)

    def send_LoRa_message(self,msg_data,in_time):
        
        sleep(in_time)
        print("Ejecutar mensaje")
        resultado_envio = -1
        try:                     
            
            self.conexion_LoRa.setblocking(False)
            #self.conexion_LoRa.send(bytes([0x01,0x02,0x03]))
            msg = self.convert_string_to_arraybytes(msg_data)
            
            ##Medir tiempo de envio
            chrono.reset()
            chrono.start()
            resultado_envio = self.conexion_LoRa.send(msg)
            chrono.stop()
            tiempo_transcurrido=chrono.read()
            
            self.mensajes_enviados +=1
            self.mensaje_id_count += 1

            if(self.debug_mode == "on"):
                self.debuging_LoRa("uplink",msg_data)
            print("Codigo ",resultado_envio)
           
            self.mqtt_client.publish(TOPIC_SEND_MESSAGE_LORA[:7]+NODO.ID_NODO+TOPIC_SEND_MESSAGE_LORA[8:],'{"nodo":'+str(NODO.ID_NODO)+'"id":'+str(self.mensaje_id_count)+',"msg_data":'+msg_data+',"latencia":'+str(tiempo_transcurrido)+'}')
            print("topic=",TOPIC_SEND_MESSAGE_LORA[:7]+NODO.ID_NODO+TOPIC_SEND_MESSAGE_LORA[8:],'{"nodo":'+str(NODO.ID_NODO)+'"id":'+str(self.mensaje_id_count)+',"msg_data":'+msg_data+',"latencia":'+str(tiempo_transcurrido)+'}')
        except OSError as e:
            if e.args[0] == 11:
                print("Error posible sobrepaso de ciclo de trabajo.", e)
                print("Devuta de mensaje: ",resultado_envio)
                
                # EAGAIN error occurred, add your retry logic here
    
    def send_LoRa_message_every(self,msg_data,cycle_time,secs_programado):
        self.estado_send_cyclic_message = True

        espera = utime.ticks_diff( secs_programado, utime.time() + timezone)
        print("espera antes del ciclo: {}s".format(espera))

        if (espera > 0):
            sleep(espera)
            
        #Empezara a enviar en el caso de que no haya error el el tiempo introducido en la programacion
        while(self.estado_send_cyclic_message): 
            self.send_LoRa_message(msg_data,0)
            sleep(cycle_time)
        print("Fin del envio ciclico de mensajes.")
        self.estado_send_cyclic_message = False      

            
    def cancel_LoRa_cyclic_message(self):

        if(self.estado_send_cyclic_message):
            print("cancelando envio ciclico de mensajes...")
            self.estado_send_cyclic_message = False
            return 1
        else: 
            print("No es posible cancelarlo.")
            return -1
    

    def debug_LoRa(self,estado):
        self.debug_mode = estado

    def debuging_LoRa(self,tipo_mensaje,data):
         
        t = utime.localtime( utime.time() + timezone)     
        fecha =  'Fecha: {:02d}:{:02d}:{:02d} {:02d}/{:02d}/{:02d}'.format(t[3], t[4], t[5],t[2],t[1],t[0])
        self.mqtt_client.publish("debug","Nodo:"+NODO.ID_NODO+"| tipo: "+tipo_mensaje +" | identificador_lora: "+self.identificador_LoRa+" | "+"id_msg: "+str(self.mensaje_id_count)+" | mensaje : "+data+" | "+fecha+"|" )
    
    def get_LoRa_status(self):
        
        datos_mostrar = [self.mensajes_enviados,self.mensajes_recibidos] 
        print(datos_mostrar)
        
        return datos_mostrar
        
    def convert_arraybytes_to_string(self,msg_data):
        data=msg_data.decode("utf-8")        
       
        return data
    def convert_string_to_arraybytes(self,msg_data):
        data = msg_data.encode("utf-8")

        return data
    
    
         

    def programar_tiempo(self,hora, minuto):
        error = -1
        fecha_actual = utime.localtime(utime.mktime(utime.localtime()) + timezone)
        t = (fecha_actual[0], fecha_actual[1], fecha_actual[2], hora, minuto, 0, fecha_actual[6], fecha_actual[7])   
        secs_programado = utime.mktime(t)
        secs_restante = utime.ticks_diff( utime.time() + timezone,secs_programado)

        if(secs_restante >= 0):
            print('Fecha: {:02d}:{:02d}:{:02d} {:02d}/{:02d}/{:02d}'.format(t[3], t[4], t[5],t[2],t[1],t[0]))
            print("segundos restantes:",secs_restante)
            #sleep(secs_restante)
            error = secs_programado
        else:
            print("Error la hora programada es anterior a la actual.")
            
            error = -1
            
        return error    
  
    
