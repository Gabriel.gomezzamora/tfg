#!/usr/bin/env python
#
# Copyright (c) 2019, Pycom Limited.
#
# This software is licensed under the GNU GPL version 3 or any
# later version, with permitted additional terms. For more information
# see the Pycom Licence v1.0 document supplied with this file, or
# available at https://www.pycom.io/opensource/licensing
#
from network import LoRa, WLAN
import machine
import pycom
import time




WIFI_SSID = "MiFibra-8614"
WIFI_PW = "aGDLAUi5"
SERVER_IP = "192.168.1.112"
BROKER_ADDR = "192.168.1.112"

RED = 0xFF0000
YELLOW = 0xFFFF33
GREEN = 0x007F00
OFF = 0x000000

def set_led_to(color=GREEN):
    pycom.heartbeat(False) # Disable the heartbeat LED
    pycom.rgbled(color)

def flash_led_to(color=GREEN, t1=1):
    set_led_to(color)
    time.sleep(t1)
    set_led_to(OFF)

	
def connect_to_wifi(wifi_ssid, wifi_passwd):
	wlan = WLAN(mode=WLAN.STA)

	for ltry in range(3):
	    print("Connecting to: "+wifi_ssid+". Try "+str(ltry))
	    nets = wlan.scan()
            time.sleep(4)
	    for net in nets:
	        if net.ssid == wifi_ssid:
	            print('Network '+wifi_ssid+' found!')
	            wlan.connect(net.ssid, auth=(net.sec, wifi_passwd), timeout=5000)
	            while not wlan.isconnected():
	                machine.idle() # save power while waiting
	            print('WLAN connection succeeded!')
	            flash_led_to(GREEN, 1)
	            print (wlan.ifconfig())
	            break
	    if wlan.isconnected():
	        break
	    else:
	        print('Cannot find network '+wifi_ssid)
	        flash_led_to(RED, 1)
	
	if not wlan.isconnected():
	    print('Cannot connect to network '+wifi_ssid+'. Quitting!!!')
	    sys.exit(1)
	
	return wlan
