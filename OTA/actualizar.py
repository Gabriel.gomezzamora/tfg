import paho.mqtt.client as mqtt
import time
import os
import shutil
from distutils.version import LooseVersion
import sys
import argparse

broker_address = "localhost"
broker_port = 1883
topic = "lopy/peticion"




def crear_directorio():
    with os.scandir("./") as ficheros:
        subdirectorios = [fichero.name for fichero in ficheros if fichero.is_dir()]
   
    lv = [LooseVersion(ver) for ver in subdirectorios]
    lv.sort()
    #print(lv)
    ultima_version = lv[len(lv)-1].vstring
    nueva_version = ultima_version.split(".")
    
    print("Ultima version:",ultima_version)
    
    nueva_version[len(nueva_version)-1] = str(int(nueva_version[len(nueva_version)-1]) + 1) 
    directorio_nueva_version ='.'.join(nueva_version)

    print("Nueva version: ",directorio_nueva_version)
    shutil.copytree(ultima_version, directorio_nueva_version)
    print("Modifica el codigo en el directorio: \n",os.getcwd(),"/",directorio_nueva_version)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("opcion" , help="Introduce 1 -> crear nuevo directorio de nueva version, Introduce 2 -> cargar actualizacion.")
    #parser.add_argument("nodo" , help="Introduce el numero del nodo que quieres actualizar o un 0 vacio para actualizar todos los nodos.")
    args = parser.parse_args()
    print(args.opcion)  

    client = mqtt.Client('Publicador_ejem1') # Creación del client
    client.connect(broker_address)


    if(int(args.opcion) == 1):
        crear_directorio()
        
    if(int(args.opcion) == 2): 
        client.publish(topic, '{"nodo":"","clave":1}')
        
            
        
   
        




sys.exit(0)

